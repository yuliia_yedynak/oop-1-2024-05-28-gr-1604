# Створіть клас, який описує книгу. Він повинен містити інформацію про автора,
# назву, рік видання та жанрі. Створіть кілька книжок. Визначте для нього
# операції перевірки на рівність та нерівність, методи __repr__ та __str__.


class Author:
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name

    def __eq__(self, other):
        if not isinstance(other, Author):
            raise TypeError(f"Cannot compare Author with {type(other)}")
        return self.last_name == other.last_name and self.first_name == other.first_name

    def __ne__(self, other):
        return not self == other

    def __str__(self):
        return f"{self.last_name} {self.first_name}"


class Book:
    def __init__(self, authors: list[Author], title: str, year: int, genre: str):
        self.authors = authors
        self.title = title
        self.year = year
        self.genre = genre

    def __eq__(self, other):
        if not isinstance(other, Book):
            raise TypeError(f"Cannot compare Book with {type(other)}")
        return self.authors == other.authors and self.title == other.title

    def __ne__(self, other):
        return not self == other

    def __repr__(self):
        return f"Book({self.author}, {self.title}, {self.year}, {self.genre})"

    def __str__(self):
        return f"{self.title} by {self.author} ({self.year})"